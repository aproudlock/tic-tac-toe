#pragma once

using namespace std;


class TicTacToe {


private:

	char m_board[3][3] = { {'1', '2', '3'}, {'4', '5', '6'}, {'7', '8', '9'}}; // Try one dimensional array

	int m_numTurns = 0;


	char m_playerTurn;
							// CHAR MUST HAVE SINGLE QUOTES ' '
	char m_winner = '0';
	

public:

	TicTacToe()
	{

	}

	void DisplayBoard() 
	{
		cout << " " << m_board[0][0] << " | " << m_board[0][1] << " | " << m_board[0][2] << "\n";
		cout << "-----------\n";
		cout << " " << m_board[1][0] << " | " << m_board[1][1] << " | " << m_board[1][2] << "\n";
		cout << "-----------\n";
		cout << " " << m_board[2][0] << " | " << m_board[2][1] << " | " << m_board[2][2] << "\n";
	}

	

	char GetPlayerTurn()
	{
		
		if (m_playerTurn == 'O') { m_playerTurn = 'X'; }
		else { m_playerTurn = 'O'; }
		
		return m_playerTurn;
	}
	

	void Move(int position)
	{

		int row = position / 3;
		int col;

		// if position is a multiple of three then the column will be 2
		if (position % 3 == 0)
		{
			row = row - 1;
			col = 2;
		}
		else col = (position % 3) - 1;

		m_board[row][col] = m_playerTurn;

		m_numTurns++;
	}

	bool IsValidMove(int position)
	{
		if(position == 'X' || position == 'O')
		{ 
			cout << "That spot has already been taken!\n";

			return false; 
		}
		else 
		{
			return true;
		}
	}

	void DisplayResult()
	{
			cout << "Player " << m_playerTurn << " is the winner!\n";
	}

	bool IsOver() 
	{

		for (int i = 0; i < 3; i++)
		{
			//rows
			if (m_board[i][0] == m_board[i][1] && m_board[i][1] == m_board[i][2])
			{
				return m_playerTurn;
			}

			//columns
			if (m_board[0][i] == m_board[1][i] && m_board[1][i] == m_board[2][i])
			{
				return m_playerTurn;
			}
		}
		if (m_board[0][0] == m_board[1][1] && m_board[1][1] == m_board[2][2])
		{
			return m_playerTurn;
		}
		if (m_board[0][2] == m_board[1][1] && m_board[1][1] == m_board[2][0])
		{
			return m_playerTurn;
		}

		return false;
		
	}



};